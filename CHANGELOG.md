# CHANGELOG

## 1.0.2

### Fixed

*   Added distribution statement.

## 1.0.1

### Fixed

*   When dynamic alpha was used, the initial static alpha was not being set.
    The moment before the dynamic animations were loaded, the objects were
    displayed with the wrong alphas.
